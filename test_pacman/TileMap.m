//
//  TileMap.m
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 06.04.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#import "TileMap.h"
#import "Tile.h"
#import "Sprite.h"

// MAP HARDCODED
static int mapMatrix[21][19] =  {
                                    {1, 1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1},
                                    {1, 10, 10, 10, 10, 10, 10, 10, 10, 1,  10, 10, 10, 10, 10, 10, 10, 10, 1},
                                    {1, 10, 1,  1,  10, 1,  1,  1,  10, 1,  10, 1,  1,  1,  10, 1,  1,  10, 1},
                                    {1, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 1},
                                    {1, 10, 1,  1,  10, 1,  10, 1,  1,  1,  1,  1,  10, 1,  10, 1,  1,  10, 1},
                                    {1, 10, 10, 10, 10, 1,  10, 10, 10, 1,  10, 10, 10, 1,  10, 10, 10, 10, 1},
                                    {1, 1,  1,  1,  10, 1,  1,  1,  0,  1,  0,  1,  1,  1,  10, 1,  1,  1,  1},
                                    {0, 0,  0,  1,  10, 1,  0,  0,  0,  13, 0,  0,  0,  1,  10, 1,  0,  0,  0},
                                    {0, 0,  0,  1,  10, 1,  0,  1,  1,  2,  1,  1,  0,  1,  10, 1,  0,  0,  0},
                                    {0, 0,  0,  1,  10, 0,  0,  1,  13, 13, 13, 1,  0,  0,  10, 1,  0,  0,  0},
                                    {0, 0,  0,  1,  10, 1,  0,  1,  1,  1,  1,  1,  0,  1,  10, 1,  0,  0,  0},
                                    {0, 0,  0,  1,  10, 1,  0,  0,  0,  0,  0,  0,  0,  1,  10, 1,  0,  0,  0},
                                    {1, 1,  1,  1,  10, 1,  0,  1,  1,  1,  1,  1,  0,  1,  10, 1,  1,  1,  1},
                                    {1, 10, 10, 10, 10, 10, 10, 10, 10, 1,  10, 10, 10, 10, 10, 10, 10, 10, 1},
                                    {1, 10, 1,  1,  10, 1,  1,  1,  10, 1,  10, 1,  1,  1,  10, 1,  1,  10, 1},
                                    {1, 10, 10, 1,  10, 10, 10, 10, 10, 12, 10, 10, 10, 10, 10, 1,  10, 10, 1},
                                    {1, 1,  10, 1,  10, 1,  10, 1,  1,  1,  1,  1,  10, 1,  10, 1,  10, 1,  1},
                                    {1, 10, 10, 10, 10, 1,  10, 10, 10, 1,  10, 10, 10, 1,  10, 10, 10, 10, 1},
                                    {1, 10, 1,  1,  1,  1,  1,  1,  10, 1,  10, 1,  1,  1,  1,  1,  1,  10, 1},
                                    {1, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 1},
                                    {1, 1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1}
                                };

@interface TileMap ()
- (void)setupMap;
@end

@implementation TileMap
@synthesize mapSize = _mapSize, tileSize = _tileSize;

#pragma mark - initialization
- (id)initWithEffect:(GLKBaseEffect *)effect
{
    if (self = [super initWithEffect:effect]) {
        [self setupMap];
    }
    return self;
}

- (void)setupMap
{
    _tileSize = CGSizeMake(15, 15);
    
    int rows = sizeof(mapMatrix)/sizeof(mapMatrix[0]);
    int columns = sizeof(mapMatrix[0])/sizeof(mapMatrix[0][0]);
    _mapSize = CGSizeMake(_tileSize.width * columns, _tileSize.height * rows);

    NSMutableArray *tiles = [NSMutableArray array];
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            Tile *tile = [Tile tileWithEffect:_effect forTag:mapMatrix[i][j]];
            tile.coords = CGPointMake(j, i);
            tile.contentSize = _tileSize;
            tile.position = GLKVector2Make(j * _tileSize.height + _tileSize.height/2, ((rows - 1) - i) * _tileSize.width + _tileSize.width/2);
            [tiles addObject:tile];
            [self addChild:tile];
        }
    }
    _tiles = tiles;
}

#pragma mark - map objects
- (void)iterateMapMatrixWithBlock:(void(^)(int i, int j, int rows, int columns, BOOL *stop))iterationBlock
{
    int rows = sizeof(mapMatrix)/sizeof(mapMatrix[0]);
    int columns = sizeof(mapMatrix[0])/sizeof(mapMatrix[0][0]);
    BOOL stop = NO;
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            iterationBlock(i, j, rows, columns, &stop);
            if (stop == YES)
                break;
        }
    }
}

- (GLKVector2)playerSpawnPoint
{
    __block GLKVector2 resultPoint;
    [self iterateMapMatrixWithBlock:^(int i, int j, int rows, int columns, BOOL *stop){
        if (mapMatrix[i][j] == sPlayerSpawnPoint) {
            resultPoint = GLKVector2Make(j * _tileSize.height + _tileSize.height/2, ((rows - 1) - i) * _tileSize.width + _tileSize.width/2);
            *stop = YES;
        }
    }];
    return resultPoint;
}

- (NSArray *)enemySpawnPoints
{
    __block NSMutableArray *resultArray = [NSMutableArray array];
    [self iterateMapMatrixWithBlock:^(int i, int j, int rows, int columns, BOOL *stop){
        if (mapMatrix[i][j] == EnemySpawnPoint) {
            CGPoint resultPoint = CGPointMake(j * _tileSize.height + _tileSize.height/2, ((rows - 1) - i) * _tileSize.width + _tileSize.width/2);
            [resultArray addObject:[NSValue valueWithCGPoint:resultPoint]];
        }
    }];
    return resultArray;
}

- (NSArray *)dotPointsArray
{
    __block NSMutableArray *resultArray = [NSMutableArray array];
    [self iterateMapMatrixWithBlock:^(int i, int j, int rows, int columns, BOOL *stop){
        if (mapMatrix[i][j] == sTagDot) {
            CGPoint resultPoint = CGPointMake(j * _tileSize.height + _tileSize.height/2, ((rows - 1) - i) * _tileSize.width + _tileSize.width/2);
            [resultArray addObject:[NSValue valueWithCGPoint:resultPoint]];
        }
    }];
    return resultArray;
}

#pragma mark - collision detection methods
- (Tile *)tileForPosition:(CGPoint)position
{
    CGPoint tileCoord = [self tileCoordForPosition:position];
    NSUInteger tileIndex = (int)(_mapSize.width/_tileSize.width) * tileCoord.y + tileCoord.x;
    return [_tiles objectAtIndex:tileIndex];
}

- (CGPoint)tileCoordForPosition:(CGPoint)position
{
    int x = position.x/_tileSize.width;
    int y = (_mapSize.height/_tileSize.height - 1) - floor(position.y/_tileSize.height);
    return CGPointMake(x, y);
}

- (NSArray *)tilesCollidingWithRect:(CGRect)rect
{
    NSMutableSet *collidingTiles = [NSMutableSet set];
    [collidingTiles addObject:[self tileForPosition:CGPointMake(rect.origin.x, rect.origin.y)]];
    [collidingTiles addObject:[self tileForPosition:CGPointMake(rect.origin.x, rect.origin.y + rect.size.height)]];
    [collidingTiles addObject:[self tileForPosition:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y)]];
    [collidingTiles addObject:[self tileForPosition:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height)]];
    return collidingTiles.allObjects;
}

- (NSArray *)tilesCollidingWithSprite:(Sprite *)sprite
{
    return [self tilesCollidingWithSprite:sprite direction:sprite.moveVelocity];
}

- (NSArray *)tilesCollidingWithSprite:(Sprite *)sprite direction:(GLKVector2)moveVelocity
{
    NSMutableSet *collidingTiles = [NSMutableSet set];
    
    CGRect spriteRect = CGRectMake(sprite.position.x - sprite.contentSize.width/2, sprite.position.y - sprite.contentSize.height/2, sprite.contentSize.width, sprite.contentSize.height);
    // right
    if (moveVelocity.x > 0) {
        [collidingTiles addObject:[self tileForPosition:CGPointMake(spriteRect.origin.x + spriteRect.size.width, spriteRect.origin.y)]];
        [collidingTiles addObject:[self tileForPosition:CGPointMake(spriteRect.origin.x + spriteRect.size.width, spriteRect.origin.y + spriteRect.size.height)]];
    }
    // left
    if (moveVelocity.x < 0) {
        [collidingTiles addObject:[self tileForPosition:CGPointMake(spriteRect.origin.x, spriteRect.origin.y)]];
        [collidingTiles addObject:[self tileForPosition:CGPointMake(spriteRect.origin.x, spriteRect.origin.y + spriteRect.size.height)]];
    }
    // up
    if (moveVelocity.y > 0) {
        [collidingTiles addObject:[self tileForPosition:CGPointMake(spriteRect.origin.x, spriteRect.origin.y + spriteRect.size.height)]];
        [collidingTiles addObject:[self tileForPosition:CGPointMake(spriteRect.origin.x + spriteRect.size.width, spriteRect.origin.y + spriteRect.size.height)]];
    }
    // down
    if (moveVelocity.y < 0) {
        [collidingTiles addObject:[self tileForPosition:CGPointMake(spriteRect.origin.x, spriteRect.origin.y)]];
        [collidingTiles addObject:[self tileForPosition:CGPointMake(spriteRect.origin.x + spriteRect.size.width, spriteRect.origin.y)]];
    }
    return collidingTiles.allObjects;
}

- (BOOL)checkDirection:(SMoveDirection)moveDirection forSprite:(Sprite *)sprite
{
    BOOL result = YES;
    NSArray *collidingTiles = nil;
    switch (moveDirection) {
        case sDirectionUp:
            collidingTiles = [self tilesCollidingWithRect:CGRectMake(sprite.position.x - sprite.contentSize.width/2 , sprite.position.y - sprite.contentSize.height/2 + sprite.contentSize.height, sprite.contentSize.width, sprite.contentSize.height)];
            break;
        case sDirectionDown:
            collidingTiles = [self tilesCollidingWithRect:CGRectMake(sprite.position.x - sprite.contentSize.width/2 , sprite.position.y - sprite.contentSize.height/2 - sprite.contentSize.height, sprite.contentSize.width, sprite.contentSize.height)];
            break;
        case sDirectionLeft:
            collidingTiles = [self tilesCollidingWithRect:CGRectMake(sprite.position.x - sprite.contentSize.width/2 - sprite.contentSize.width, sprite.position.y - sprite.contentSize.height/2, sprite.contentSize.width, sprite.contentSize.height)];
            break;
        case sDirectionRight:
            collidingTiles = [self tilesCollidingWithRect:CGRectMake(sprite.position.x - sprite.contentSize.width/2 + sprite.contentSize.width, sprite.position.y - sprite.contentSize.height/2, sprite.contentSize.width, sprite.contentSize.height)];
            break;
        case sDirectionNone:
            break;
    }
    
    for (Tile *tile in collidingTiles) {
        if ([[tile.properties valueForKey:@"Door"] boolValue] && (tile.position.y > sprite.position.y)) {
            continue;
        }
        if ([[tile.properties valueForKey:@"Collidable"] boolValue] == YES) {
            result = NO;
            break;
        }
    }
    return result;
}

@end
