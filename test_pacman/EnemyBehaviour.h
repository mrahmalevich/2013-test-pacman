//
//  EnemyBehaviour.h
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 09.04.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Sprite, Enemy, TileMap;
@interface EnemyBehaviour : NSObject

- (void)makeDecisionForEnemy:(Enemy *)enemy player:(Sprite *)player andMap:(TileMap *)map;

@end
