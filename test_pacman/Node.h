//
//  Node.h
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 03.04.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>

@interface Node : NSObject {
@protected
    GLKBaseEffect *_effect;
    BOOL _showControls;
}

@property (strong, readonly) GLKBaseEffect *effect;
@property (assign) GLKVector2 position;
@property (assign) CGSize contentSize;
@property (assign) GLKVector2 moveVelocity;
@property (strong) NSMutableArray * children;
@property (assign) float rotation;
@property (assign) float scale;
@property (assign) float rotationVelocity;
@property (assign) float scaleVelocity;
@property (readonly) BOOL showControls;

- (id)initWithEffect:(GLKBaseEffect *)effect;
- (void)renderWithModelViewMatrix:(GLKMatrix4)modelViewMatrix;
- (void)update:(float)dt;
- (GLKMatrix4) modelMatrix:(BOOL)renderingSelf;
- (CGRect)boundingBox;
- (void)addChild:(Node *)child;
- (void)handleTap:(CGPoint)touchLocation;
- (void)handleControlWithTag:(NSInteger)tag;

@end
