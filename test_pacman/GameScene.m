//
//  GameScene.m
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 06.04.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#import "GameScene.h"
#import "GameOverScene.h"

#import "AppDelegate.h"
#import "ViewController.h"

#import "Sprite.h"
#import "Enemy.h"
#import "Tile.h"
#import "TileMap.h"
#import "SpriteAnimation.h"

@interface GameScene ()
- (void)activateEnemy:(Enemy *)enemy;
@end

@implementation GameScene

- (id)initWithEffect:(GLKBaseEffect *)effect
{
    if (self = [super initWithEffect:effect]) {
        // map
        _tileMap = [[TileMap alloc] initWithEffect:effect];
        [self addChild:_tileMap];
        
        // player
        _player = [[Character alloc] initWithFile:@"pacman.png" effect:self.effect];
        _player.contentSize = CGSizeMake(13, 13);
        _player.position = [_tileMap playerSpawnPoint];
        _player.direction = sDirectionNone;
        _playerDirectionMask = sDirectionNone;
        [self handlePlayerDirection];
        [self addChild:_player];
        
        // dots
        _dots = [NSMutableArray array];
        NSArray *dotPoints = [_tileMap dotPointsArray];
        for (NSValue *dotValue in dotPoints) {
            CGPoint dotPoint = dotValue.CGPointValue;
            Sprite *dot = [[Sprite alloc] initWithFile:@"dot.png" effect:self.effect];
            dot.position = GLKVector2Make(dotPoint.x, dotPoint.y);
            [_dots addObject:dot];
            [self addChild:dot];
        }

        // enemies
        _enemies = [NSMutableArray array];
        NSArray *enemyPoints = [_tileMap enemySpawnPoints];
        EnemyName enemyName = 0;
        for (NSValue *spawnPointValue in enemyPoints) {
            CGPoint spawnPoint = spawnPointValue.CGPointValue;
            Enemy *enemy = [[Enemy alloc] initWithFile:@"blinky-down_1.png" effect:self.effect];
            enemy.name = enemyName++;
            enemy.position = GLKVector2Make(spawnPoint.x, spawnPoint.y);
            [_enemies addObject:enemy];
            [self handleEnemyDirection:enemy];
            [self addChild:enemy];
        }
        _activeEnemies = [NSMutableArray array];
        Enemy *firstEnemy = [_enemies objectAtIndex:0];
        [self activateEnemy:firstEnemy];
        
        self.contentSize = _tileMap.mapSize;
        self.position = GLKVector2Make(100, 2.5);
        
        _showControls = YES;
        _gameOver = NO;
    }
    return self;
}

- (void)update:(float)dt
{
    // storing old player & enemies positions
    GLKVector2 prevPlayerPosition = _player.position;
    NSMutableArray *prevEnemyPositions = [NSMutableArray array];
    for (Enemy *enemy in _activeEnemies) {
        [prevEnemyPositions addObject:[NSValue valueWithCGPoint:CGPointMake(enemy.position.x, enemy.position.y)]];
    }
    
    // updating all nodes
    [super update:dt];
    if (_gameOver)
        return;
    
    // catching by enemy
    for (Node *enemy in _enemies) {
        if (CGRectContainsPoint(_player.boundingBox, CGPointMake(enemy.position.x, enemy.position.y))) {
            [self gameOver:NO];
            break;
        }
    }
    
    // picking-up dots
    for (Node *dot in _dots) {
        if (CGRectIntersectsRect(dot.boundingBox, _player.boundingBox)) {
            [_dots removeObject:dot];
            [self.children removeObject:dot];
            if (_dots.count == 0) {
                [self gameOver:YES];
            }
            break;
        }
    }
    
    
    // player movement
    // # processing controls
    BOOL decisionMade = NO;
    if (_playerDirectionMask != _player.direction) {
        SMoveDirection directionToCheck = 1;
        while (directionToCheck <= _playerDirectionMask) {
            if ((directionToCheck | _playerDirectionMask) == _playerDirectionMask && (directionToCheck != _player.direction)) {
                BOOL directionAvailable = [_tileMap checkDirection:directionToCheck forSprite:_player];
                if (directionAvailable) {
                    _player.direction = _playerDirectionMask = directionToCheck;
                    [self handlePlayerDirection];
                    decisionMade = YES;
                }
            }
            directionToCheck = directionToCheck << 1;
        }
        
    }
    // # processing player wall collisions
    if (!decisionMade) {
        NSArray *collidingTiles = [_tileMap tilesCollidingWithSprite:_player];
        for (Tile *tile in collidingTiles) {
            if ([[tile.properties valueForKey:@"Collidable"] boolValue]) {
                _player.position = prevPlayerPosition;
                _playerDirectionMask = _playerDirectionMask ^ _player.direction;
                _player.direction = sDirectionNone;
                break;
            }
        }
    }
    // # updating velocity
    switch (_player.direction) {
        case sDirectionUp:
            _player.moveVelocity = GLKVector2Make(0, PLAYER_SPEED);
            break;
        case sDirectionDown:
            _player.moveVelocity = GLKVector2Make(0, -PLAYER_SPEED);
            break;
        case sDirectionLeft:
            _player.moveVelocity = GLKVector2Make(-PLAYER_SPEED, 0);
            break;
        case sDirectionRight:
            _player.moveVelocity = GLKVector2Make(PLAYER_SPEED, 0);
            break;
        case sDirectionNone:
            _player.moveVelocity = GLKVector2Make(0, 0);
            break;
    }
    
    // enemies movement
    // # pending enemy position
    if (_pendingEnemy) {
        if ([_tileMap checkDirection:sDirectionUp forSprite:_pendingEnemy]) {
            _pendingEnemy.direction = sDirectionNone;
        }
    }
    // # active enemies
    for (Enemy *enemy in _activeEnemies) {
        BOOL decisionMade = NO;
        // # wall collisions
        NSArray *collidingTiles = [_tileMap tilesCollidingWithSprite:enemy];
        for (Tile *tile in collidingTiles) {
            if ([[tile.properties valueForKey:@"Collidable"] boolValue]) {
                if ([[tile.properties valueForKey:@"Door"] boolValue] && (tile.position.y > enemy.position.y))
                    continue;
                NSValue *prevPositionValue = [prevEnemyPositions objectAtIndex:[_activeEnemies indexOfObject:enemy]];
                GLKVector2 prevPosition = GLKVector2Make(prevPositionValue.CGPointValue.x, prevPositionValue.CGPointValue.y);
                [enemy setPosition:prevPosition];
                [enemy makeDecisionForPlayer:_player andMap:_tileMap];
                [self handleEnemyDirection:enemy];
                decisionMade = YES;
                break;
            }
        }
        if (decisionMade)
            continue;
        // # decision points
        BOOL makeDecision = NO;
        if (enemy.moveVelocity.x != 0) {
            makeDecision = [_tileMap checkDirection:sDirectionUp forSprite:enemy] || [_tileMap checkDirection:sDirectionDown forSprite:enemy];
        } else {
            makeDecision = [_tileMap checkDirection:sDirectionLeft forSprite:enemy] || [_tileMap checkDirection:sDirectionRight forSprite:enemy];
        }
        if (makeDecision) {
            [enemy makeDecisionForPlayer:_player andMap:_tileMap];
            [self handleEnemyDirection:enemy];
        }
    }
    // # updating enemies velocity
    for (Enemy *enemy in _enemies) {
        switch (enemy.direction) {
            case sDirectionUp:
                enemy.moveVelocity = GLKVector2Make(0, ENEMY_SPEED);
                break;
            case sDirectionDown:
                enemy.moveVelocity = GLKVector2Make(0, -ENEMY_SPEED);
                break;
            case sDirectionLeft:
                enemy.moveVelocity = GLKVector2Make(-ENEMY_SPEED, 0);
                break;
            case sDirectionRight:
                enemy.moveVelocity = GLKVector2Make(ENEMY_SPEED, 0);
                break;
            case sDirectionNone:
                enemy.moveVelocity = GLKVector2Make(0, 0);
                break;
        }
    }
}

- (void)activateEnemy:(Enemy *)enemy
{
    // reset penging enemy
    _pendingEnemy = nil;

    // starting player chase
    enemy.direction = sDirectionUp;
    [enemy setMoveVelocity:GLKVector2Make(0, ENEMY_SPEED)];
    [_activeEnemies addObject:enemy];

    if (_activeEnemies.count < _enemies.count) {
        // we choose next enemy
        NSMutableArray *inactiveEnemies = [NSMutableArray arrayWithArray:_enemies];
        [inactiveEnemies removeObjectsInArray:_activeEnemies];
        // check activation point
        for (Enemy *aenemy in inactiveEnemies) {
            CGPoint enemyCoords = [_tileMap tileForPosition:CGPointMake(aenemy.position.x, aenemy.position.y)].coords;
            if (CGPointEqualToPoint(enemyCoords, ENEMY_ACTIVATION_POINT)) {
                _pendingEnemy = aenemy;
                break;
            }
        }
        // if there is no enemy in activation point we move one
        if (!_pendingEnemy) {
            _pendingEnemy = [inactiveEnemies lastObject];
            SMoveDirection pendingEnemyDirection = sDirectionNone;
            if (_pendingEnemy.position.x < ENEMY_ACTIVATION_POINT.x * _tileMap.tileSize.width) {
                pendingEnemyDirection = sDirectionRight;
            } else {
                pendingEnemyDirection = sDirectionLeft;
            }
            _pendingEnemy.direction = pendingEnemyDirection;
        }
        // and start timer
        [NSTimer scheduledTimerWithTimeInterval:ENEMY_ACTIVATION_PERIOD target:self selector:@selector(activateNextEnemy)  userInfo:nil repeats:NO];
    }
}

- (void)activateNextEnemy
{
    [self activateEnemy:_pendingEnemy];    
}

- (void)handleControlWithTag:(NSInteger)tag
{
    switch (tag) {
        case CONTROL_UP:
            _playerDirectionMask = _playerDirectionMask | sDirectionUp;
            break;
        case CONTROL_DOWN:
            _playerDirectionMask = _playerDirectionMask | sDirectionDown;
            break;
        case CONTROL_LEFT:
            _playerDirectionMask = _playerDirectionMask | sDirectionLeft;
            break;
        case CONTROL_RIGHT:
            _playerDirectionMask = _playerDirectionMask | sDirectionRight;
            break;
        default:
            break;
    }
}

- (void)handlePlayerDirection
{
    SpriteAnimation *animation = nil;
    switch (_player.direction) {
        case sDirectionUp:
            animation = [[SpriteAnimation alloc] initWithTimePerFrame:1.0/9 framesNamed:@[@"pacman-up_1.png", @"pacman-up_2.png"]];
            break;
        case sDirectionDown:
            animation = [[SpriteAnimation alloc] initWithTimePerFrame:1.0/9 framesNamed:@[@"pacman-down_1.png", @"pacman-down_2.png"]];
            break;
        case sDirectionLeft:
            animation = [[SpriteAnimation alloc] initWithTimePerFrame:1.0/9 framesNamed:@[@"pacman-left_1.png", @"pacman-left_2.png"]];
            break;
        case sDirectionRight:
            animation = [[SpriteAnimation alloc] initWithTimePerFrame:1.0/9 framesNamed:@[@"pacman-right_1.png", @"pacman-right_2.png"]];
            break;
        case sDirectionNone:
            break;
    }
    [_player setSpriteAnimation:animation];    
}

- (void)handleEnemyDirection:(Enemy *)enemy
{
    NSString *prefix = nil;
    switch (enemy.name) {
        case sBlinky:
            prefix = @"blinky";
            break;
        case sInky:
            prefix = @"inky";
            break;
        case sPinky:
            prefix = @"pinky";
            break;
        case sClyde:
            prefix = @"clyde";
            break;
    }

    NSArray *framesArray = nil;
    switch (enemy.direction) {
        case sDirectionRight:
            framesArray = @[[NSString stringWithFormat:@"%@-right_1.png", prefix], [NSString stringWithFormat:@"%@-right_2.png", prefix]];
            break;
        case sDirectionLeft:
            framesArray = @[[NSString stringWithFormat:@"%@-left_1.png", prefix], [NSString stringWithFormat:@"%@-left_2.png", prefix]];
            break;
        case sDirectionUp:
        case sDirectionNone:
            framesArray = @[[NSString stringWithFormat:@"%@-up_1.png", prefix], [NSString stringWithFormat:@"%@-up_2.png", prefix]];
            break;
        case sDirectionDown:
            framesArray = @[[NSString stringWithFormat:@"%@-down_1.png", prefix], [NSString stringWithFormat:@"%@-down_2.png", prefix]];
            break;
    }
    [enemy setSpriteAnimation:[[SpriteAnimation alloc] initWithTimePerFrame:1.0/9 framesNamed:framesArray]];
}

- (void)gameOver:(BOOL)win
{
    _gameOver = YES;
    _playerDirectionMask = sDirectionNone;
    _player.direction = sDirectionNone;
    
    for (Enemy *enemy in _enemies) {
        [self.children removeObject:enemy];
    }
    
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    UIWindow *mainWindow = [delegate window];
    ViewController *viewController = (ViewController *)mainWindow.rootViewController;
    [viewController.vwControls setHidden:YES];

    if (win) {
        [_player setSprite:@"pacman.png"];
    } else {
        NSArray *framesArray = @[@"pacman-death_1.png", @"pacman-death_2.png", @"pacman-death_3.png", @"pacman-death_4.png", @"pacman-death_5.png", @"pacman-death_6.png", @"pacman-death_7.png", @"pacman-death_8.png", @"pacman-death_9.png", @"pacman-death_10.png", @"pacman-death_11.png"];
        SpriteAnimation *animation = [[SpriteAnimation alloc] initWithTimePerFrame:1.0/9 framesNamed:framesArray];
        [animation setRepeat:NO];
        [_player setSpriteAnimation:animation];
    }
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        GameOverScene *scene = [[GameOverScene alloc] initWithEffect:self.effect win:win];
        viewController.scene = scene;
    });
}
        


@end
