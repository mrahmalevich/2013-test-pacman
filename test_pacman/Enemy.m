//
//  SCharacter.m
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 07.04.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#import "Enemy.h"
#import "EnemyBehaviour.h"

@implementation Enemy
@synthesize name;

- (id)initWithFile:(NSString *)fileName effect:(GLKBaseEffect *)effect
{
    if (self = [super initWithFile:fileName effect:effect]) {
        _behaviour = [[EnemyBehaviour alloc] init];
    }
    return self;
}

- (void)makeDecisionForPlayer:(Sprite *)player andMap:(TileMap *)map
{
    [_behaviour makeDecisionForEnemy:self player:player andMap:map];
}

@end
