//
//  GameOverScene.m
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 04.04.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#import "GameOverScene.h"
#import "Sprite.h"
#import "ViewController.h"
#import "GameScene.h"
#import "AppDelegate.h"

@interface GameOverScene ()
@property (assign) float timeSinceInit;
@end

@implementation GameOverScene
@synthesize timeSinceInit = _timeSinceInit;

- (id)initWithEffect:(GLKBaseEffect *)effect win:(BOOL)win {
    if ((self = [super initWithEffect:effect])) {
        if (win) {
            Sprite * winSprite = [[Sprite alloc] initWithFile:@"YouWin.png" effect:effect];
            winSprite.position = GLKVector2Make(240, 160);
            [self addChild:winSprite];
        } else {
            Sprite * loseSprite = [[Sprite alloc] initWithFile:@"YouLose.png" effect:effect];
            loseSprite.position = GLKVector2Make(240, 160);
            [self addChild:loseSprite];
        }
        
    }
    return self;
}

- (void)update:(float)dt {
    
    self.timeSinceInit += dt;
    if (self.timeSinceInit > 3.0) {
        GameScene * scene = [[GameScene alloc] initWithEffect:self.effect];
        AppDelegate * delegate = [[UIApplication sharedApplication] delegate];
        UIWindow * mainWindow = [delegate window];
        ViewController * viewController = (ViewController *) mainWindow.rootViewController;
        viewController.scene = scene;
    }
    
}

@end
