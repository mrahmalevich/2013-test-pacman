//
//  TileMap.h
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 06.04.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#import "Node.h"

@class Sprite, Tile;
@interface TileMap : Node {
@private
    NSArray *_tiles;
    CGSize _mapSize;
    CGSize _tileSize;
}

@property (nonatomic, readonly) CGSize mapSize;
@property (nonatomic, readonly) CGSize tileSize;

- (id)initWithEffect:(GLKBaseEffect *)effect;

- (GLKVector2)playerSpawnPoint;
- (NSArray *)enemySpawnPoints;
- (NSArray *)dotPointsArray;

- (Tile *)tileForPosition:(CGPoint)position;
- (NSArray *)tilesCollidingWithRect:(CGRect)rect;
- (NSArray *)tilesCollidingWithSprite:(Sprite *)sprite;
- (BOOL)checkDirection:(SMoveDirection)moveDirection forSprite:(Sprite *)sprite;

@end
