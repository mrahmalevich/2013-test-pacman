//
//  GameOverScene.h
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 04.04.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#import "Node.h"

@interface GameOverScene : Node

- (id)initWithEffect:(GLKBaseEffect *)effect win:(BOOL)win;

@end
