//
//  Tile.m
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 06.04.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#import "Tile.h"

@interface Tile ()

@end

@implementation Tile
@synthesize coords = _coords;

+ (Tile *)tileWithEffect:(GLKBaseEffect *)effect forTag:(SMapObjectTag)tag
{
    Tile *newTile = [[super alloc] initWithFile:[Tile imageFileFotTag:tag] effect:effect];
    [newTile setupPropertiesForTag:tag];

    return newTile;
}

+ (NSString *)imageFileFotTag:(SMapObjectTag)tag
{
    NSString *file = nil;
    switch (tag) {
        case sTagWall:
            file = @"tile_wall.png";
            break;
//        case sTagDoor:
//            file = @"tile_door.png";
//            break;
        case sTagEmpty:
        default:
            file = @"tile_empty.png";
            break;
    }
    return file;
}

- (void)setupPropertiesForTag:(SMapObjectTag)tag
{
    _properties = [NSMutableDictionary dictionary];
    switch (tag) {
        case sTagDoor:
            [_properties setValue:[NSNumber numberWithBool:YES] forKey:@"Door"];
        case sTagWall:
            [_properties setValue:[NSNumber numberWithBool:YES] forKey:@"Collidable"];
            break;
        default:
            [_properties setValue:[NSNumber numberWithBool:NO] forKey:@"Collidable"];
            [_properties setValue:[NSNumber numberWithBool:NO] forKey:@"Door"];
            break;
    }
}

@end
