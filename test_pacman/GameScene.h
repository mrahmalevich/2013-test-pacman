//
//  GameScene.h
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 06.04.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#import "Node.h"

@class TileMap, Sprite, Character, Enemy;
@interface GameScene : Node {
@private
    TileMap *_tileMap;

    Character *_player;
    NSInteger _playerDirectionMask;
    
    NSMutableArray *_dots;
    NSMutableArray *_enemies;
    NSMutableArray *_activeEnemies;
    Enemy *_pendingEnemy;
    
    BOOL _gameOver;
}

- (id)initWithEffect:(GLKBaseEffect *)effect;

@end
