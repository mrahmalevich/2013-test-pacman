//
//  ViewController.h
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 17.03.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

@class Node;
@interface ViewController : GLKViewController

@property (nonatomic, strong) Node *scene;
@property (nonatomic, strong) IBOutlet UIView *vwControls;

- (IBAction)handleControl:(UIButton *)control;

@end
