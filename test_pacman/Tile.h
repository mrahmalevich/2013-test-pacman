//
//  Tile.h
//  test_pacman
//
//  Created by Mikhail Rahmalevich on 06.04.13.
//  Copyright (c) 2013 mrahmalevich. All rights reserved.
//

#import "Sprite.h"
#import "TileMap.h"

@interface Tile : Sprite {
@private
    NSString *_image;
    NSMutableDictionary *_properties;
    CGPoint _coords;
}

@property (nonatomic, readonly) NSMutableDictionary *properties;
@property (nonatomic, assign) CGPoint coords;

+ (Tile *)tileWithEffect:(GLKBaseEffect *)effect forTag:(SMapObjectTag)tag;

@end
